﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UI.Pagination
{    
    public class PagedRect_ScrollRect : ScrollRect
    {
        public bool DisableDragging = false;
        public bool DisableScrollWheel = false;

        protected bool isBeingDragged = false;
        
        public override void OnBeginDrag(PointerEventData eventData) 
        {
            if (!DisableDragging)
            {
                var analysis = AnalyseDragEvent(eventData);                

                if (this.horizontal && analysis.DragPlane != DragEventAnalysis.eDragPlane.Horizontal) return;
                if (this.vertical && analysis.DragPlane != DragEventAnalysis.eDragPlane.Vertical) return;

                base.OnBeginDrag(eventData);

                isBeingDragged = true;
            }            
        }

        public override void OnDrag(PointerEventData eventData) 
        {
            if (!isBeingDragged) return;

            if (!DisableDragging)
            {                
                base.OnDrag(eventData);                
            }
        }
        
        public override void OnEndDrag(PointerEventData eventData) 
        {
            if (!isBeingDragged) return;

            isBeingDragged = false;

            if (!DisableDragging)
            {                
                base.OnEndDrag(eventData);                
            }
        }

        public override void OnScroll(PointerEventData data)
        {
            if (!DisableScrollWheel) base.OnScroll(data);
        }

        public DragEventAnalysis AnalyseDragEvent(PointerEventData data)
        {
            return new DragEventAnalysis(data);
        }

        public class DragEventAnalysis
        {
            private PointerEventData data;

            public enum eDragPlane
            {
                Horizontal,
                Vertical,
                None
            }

            public eDragPlane DragPlane
            {
                get
                {
                    if (Math.Abs(data.delta.x) > Math.Abs(data.delta.y))
                    {
                        return eDragPlane.Horizontal;
                    }

                    if (Math.Abs(data.delta.y) > Math.Abs(data.delta.x))
                    {
                        return eDragPlane.Vertical;
                    }

                    return eDragPlane.None;
                }
            }
            
            public DragEventAnalysis(PointerEventData data)
            {
                this.data = data;                
            }            
        }
    }
}
