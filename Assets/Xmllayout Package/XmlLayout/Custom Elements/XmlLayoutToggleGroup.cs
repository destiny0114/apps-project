﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Xml
{
    public class XmlLayoutToggleGroup : ToggleGroup
    {
        public Sprite ToggleBackgroundImage;
        public Color ToggleBackgroundColor;

        public Sprite ToggleSelectedImage;
        public Color ToggleSelectedColor;

        protected List<Toggle> m_toggleElements = new List<Toggle>();
        protected List<Action<int>> m_EventHandlers = new List<Action<int>>();

        protected int m_previousValue = -1;

// I'm not sure why but ToggleGroup.OnValidate seems to be implemented in some platforms and not in others...
#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
#else
        protected void OnValidate()
        {            
#endif

            UpdateToggleElements();
        }

        public void UpdateToggleElements()
        {
            m_toggleElements.ForEach(t => UpdateToggleElement(t));
        }

        public void UpdateToggleElement(Toggle toggle)
        {
            var xmlLayoutToggleButton = toggle.GetComponent<XmlLayoutToggleButton>();

            if (xmlLayoutToggleButton != null)
            {
                // This is a Toggle Button Element
            }
            else
            {
                // This is a Toggle Element
                var background = toggle.targetGraphic.GetComponent<Image>();
                background.sprite = ToggleBackgroundImage;
                background.color = ToggleBackgroundColor;

                var checkmark = toggle.graphic.GetComponent<Image>();
                checkmark.sprite = ToggleSelectedImage;
                checkmark.color = ToggleSelectedColor;
            }
        }

        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange();

            UpdateToggleElements();
        }

        public void AddToggle(Toggle toggle)
        {
            toggle.group = this;
            m_toggleElements.Add(toggle);
        }

        public void AddOnValueChangedEventHandler(Action handler)
        {
            m_EventHandlers.Add((e) => handler.Invoke());
        }

        public void AddOnValueChangedEventHandler(Action<int> handler)
        {
            m_EventHandlers.Add(handler);
        }

        void ValueChanged(int newValue)
        {
            if (m_EventHandlers.Any())
            {                
                m_EventHandlers.ForEach(e => e.Invoke(newValue));
            }
        }

        public int GetSelectedValue()
        {    
            for (var x = 0; x < m_toggleElements.Count; x++)
            {
                if (m_toggleElements[x].isOn) return x;
            }

            return -1;
        }

        public void SetSelectedValue(int newValue, bool fireEvent = true)
        {           
            for (var x = 0; x < m_toggleElements.Count; x++)
            {
                m_toggleElements[x].isOn = x == newValue;

                if (m_toggleElements[x].isOn && x != m_previousValue)
                {
                    if(fireEvent) ValueChanged(x);
                }
            }

            m_previousValue = newValue;
        }

        internal int GetValueForElement(Toggle element)
        {
            for (var x = 0; x < m_toggleElements.Count; x++)
            {
                if (m_toggleElements[x] == element) return x;
            }

            return -1;
        }
    }
}
