using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Xml;

namespace UI.Xml.Examples
{
    class XmlLayout_Example_HUD : XmlLayoutController
    {
        public XmlLayout_Example_ExampleMenu ExampleMenu = null;        

        public override void Hide(Action onCompleteCallback = null)
        {
            ExampleMenu.SelectExample(null);
        }
    }
}