﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections.Generic;
using System.Collections;

namespace UI.Xml.Examples
{    
    class XmlLayout_Example_MessageDialog : XmlLayoutController
    {
        Text titleText;
        Text messageText;

        void Start()
        {                        
            titleText = xmlLayout.GetElementById<Text>("titleText");
            messageText = xmlLayout.GetElementById<Text>("messageText");                   
        }

        public void Show(string title, string text)
        {
            xmlLayout.Show();

            // Because this dialog may not have been active yet at this point,
            // we need to wait a frame to make sure that the XmlLayout has finished setting up,
            // and that the titleText and messageText objects have been populated by Start()
            StartCoroutine(DelayedShow(title, text));
        }

        protected IEnumerator DelayedShow(string title, string text)
        {            
            yield return new WaitForEndOfFrame();            

            titleText.text = title;
            messageText.text = text;
        }        

        public void AppendText(string newText)
        {
            Show(this.titleText.text, messageText.text + "\r\n\r\n" + newText);            
        }
    }
}
