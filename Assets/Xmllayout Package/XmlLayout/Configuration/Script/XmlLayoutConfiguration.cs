﻿using UnityEngine;

namespace UI.Xml.Configuration
{    
    public class XmlLayoutConfiguration : ScriptableObject
    {
        public Object XSDFile;
        
        /// <summary>
        /// This is used as a base file which then dynamically has custom-attributes and tags added to it before being compiled into XSDFile.
        /// </summary>
        public Object BaseXSDFile;
    }
}
