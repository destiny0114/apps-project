﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UI.Xml;

public class MainXmlLayoutController : XmlLayoutController
{
	private bool click = false;
	
    public virtual void ButtonClicked()
    {
        Application.OpenURL("http://www.tourism.johor.my/");
    }

    public virtual void ButtonComeOut()
    {
		if (click) {
			xmlLayout.GetElementById ("animPanel").Hide ();
			click = false;
		}
		else if(!click)
		{
			xmlLayout.GetElementById("animPanel").Show();
			click = true;
		}
    }

    public virtual void SlideIn()
    {
        xmlLayout.GetElementById("animationPanel").Show();
    }

    public virtual void SlideOut()
    {
        xmlLayout.GetElementById("animationPanel").Hide();
    }
}
